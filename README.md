# Никита Куликов

[![Telegram](https://img.shields.io/badge/Telegram-black?style=social&logo=telegram)](https://t.me/expiko)
[![Outlook](https://img.shields.io/badge/Outlook-black?style=social&logo=microsoft-outlook)](mailto:expiko@outlook.com)

## Тестировщик (ручное + автоматизация)

[Резюме](https://saratov.hh.ru/resume/99dc2266ff0c3a3dc70039ed1f623948395137?hhtmFrom=resume_list)

## Ручное тестирование (Web + Mobile)

![DevTools](https://img.shields.io/badge/DevTools-122529?style=for-the-badge&logo=googlechrome)
![Postman](https://img.shields.io/badge/Postman-122529?style=for-the-badge&logo=postman&logoColor=f76935)
![Swagger](https://img.shields.io/badge/Swagger-122529?style=for-the-badge&logo=swagger&logoColor=7ede2b)
![GraphQL](https://img.shields.io/badge/GraphQL_Playground-122529?style=for-the-badge&logo=graphql&logoColor=d4088d)
![SOAPUI](https://img.shields.io/badge/SOAPUI-122529?style=for-the-badge&logo=soapui&logoColor=d4088d)
![GitLab](https://img.shields.io/badge/GitLab_Issues-122529?style=for-the-badge&logo=gitlab)
![GoogleSheets](https://img.shields.io/badge/Google%20Sheets-122529?style=for-the-badge&logo=google-sheets)
![Figma](https://img.shields.io/badge/Figma-122529?style=for-the-badge&logo=figma&logoColor=7d5fa6)
![MySQL](https://img.shields.io/badge/MySQL-122529?style=for-the-badge&logo=mysql)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-122529?style=for-the-badge&logo=postgresql)
![Dbeaver](https://img.shields.io/badge/Dbeaver-122529?style=for-the-badge&logo=dbeaver)
![Genymotion](https://img.shields.io/badge/genymotion-122529?style=for-the-badge&logo=genymotion&logoColor=3ad07d)

### Тестовые артефакты

- [Чек-листы](https://docs.google.com/spreadsheets/d/1SCehDdnDXC6ItnkbMjtPrwiPHngEMxlexO49n8b_VTE/edit?usp=sharing)
- [Тест-кейсы](https://docs.google.com/spreadsheets/d/17rIUYu4kFcD0ipJmrJry2LSaoCYu0OM5gnsXn_TdSRM/edit?usp=sharing)
- [Баг-репорты](https://gitlab.com/expiko/portfolio/-/issues)
- [Postman коллекция](https://gitlab.com/)
- [SQL-запросы](https://gitlab.com/expiko/expiko/-/tree/master/assets/SQL)

## Автоматизация

![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-122529?style=for-the-badge&logo=visual-studio-code&logoColor=0080FF)
![Playwright](https://img.shields.io/badge/Playwright-122529?style=for-the-badge&logo=playwright)
![TypeScript](https://img.shields.io/badge/typescript-122529?style=for-the-badge&logo=typescript)
![Git](https://img.shields.io/badge/Git-122529?style=for-the-badge&logo=git)
![GitLab](https://img.shields.io/badge/GitLab-122529?style=for-the-badge&logo=gitlab)

### Примеры автотестов

- [E2E](https://gitlab.com/expiko/expiko/-/tree/master/assets/Automation/Tests/E2E)
- [API](https://gitlab.com/expiko/expiko/-/tree/master/assets/Automation/Tests/API)


## Сертификаты

IT-Академия Точка Входа - [удостоверение о переквалификации](/assets/сертификат.png)

