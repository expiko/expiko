import { expect, Locator, Page } from "@playwright/test";

export class ActionItemsPage {
	readonly page: Page;
	readonly pageTitle: Locator;

	constructor(page: Page) {
		this.page = page;
		this.pageTitle = this.page.getByTestId("action-items-title");
	}

	async toHaveTitle(title: string) {
		await expect(this.pageTitle).toHaveText(title);
	}

	async toHaveURL() {
		await expect(this.page).toHaveURL(this.page.url());
	}
}
