import { expect, Locator, Page } from "@playwright/test";

export class LoginPage {
	readonly page: Page;
	readonly signInLink: Locator;
	readonly emailField: Locator;
	readonly passwordField: Locator;
	readonly loginButton: Locator;
	readonly errorMessage: Locator;
	readonly signInButton: Locator;
	private errorText: string = "Error: Authentication failed: Wrong login or password";

	constructor(page: Page) {
		this.page = page;
		this.signInLink = page.locator(".signin");
		this.emailField = page.locator("#email");
		this.passwordField = page.locator("#password");
		this.loginButton = page.locator(".button-login");
		this.signInButton = page.locator(".main .signin");
		this.errorMessage = page.locator(`text=${this.errorText}`);
	}

	async visitMyRetro() {
		await this.page.goto("/");
	}

	async login(email: string, password: string) {
		await this.signInLink.click();
		await this.emailField.type(email);
		await this.passwordField.type(password);
		await this.loginButton.click();
	}

	async signInButtonToBeVisible() {
		await expect(this.signInButton).toBeVisible();
	}

	async assertErrorMessage() {
		await expect(this.errorMessage).toContainText(this.errorText);
	}
}
