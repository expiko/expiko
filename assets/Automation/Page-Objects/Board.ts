import { expect, Locator, Page } from "@playwright/test";

export class Board {
	readonly page: Page;
	readonly card: Locator;
	readonly pageTitle: Locator;

	constructor(page: Page) {
		this.page = page;
		this.card = this.page.locator(".row .content");
		this.pageTitle = this.page.getByTestId("board-project-title");
	}

	async addCard(columnName: Column) {
		await this.page.getByRole("button", { name: columnName }).click();
	}

	async typeInCard(value: string) {
		await this.card.type(value);
	}

	async pressEnter() {
		await this.page.keyboard.press("Enter");
	}

	async assertCard(isToHaveValue: Boolean, value: string) {
		await this.isToHaveText(isToHaveValue, this.card, value);
	}

	async toHaveTitle(title: string) {
		await expect(this.pageTitle).toHaveText(title);
	}

	async toHaveURL() {
		await expect(this.page).toHaveURL(this.page.url());
	}

	private async isToHaveText(isToHaveValue: Boolean, card: Locator, value: string) {
		if (isToHaveValue) {
			await expect(card).toHaveText(value);
		} else {
			await expect(card).not.toHaveText(value);
		}
	}
}

export enum Column {
	Good = "Good",
	Bad = "Bad",
	Keep = "Keep",
	Actions = "Actions",
	WentWell = "Went well",
	ToImprove = "To improve",
	ActionItems = "Action items",
	Mad = "Mad",
	Sad = "Sad",
	Glad = "Glad",
	Start = "Start",
	Stop = "Stop",
	Continue = "Continue",
	Liked = "Liked",
	Learned = "Learned",
	Lacked = "Lacked",
	Longed = "Longed",
	Less = "Less",
	Add = "Add",
	More = "More",
}
