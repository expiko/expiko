import { Locator, Page } from "@playwright/test";

export class Header {
	readonly page: Page;
	readonly hamburgerMenu: Locator;
	readonly logoutButton: Locator;
	readonly Option: IOption;

	constructor(page: Page) {
		this.page = page;
		this.hamburgerMenu = this.page.locator(".hamburger-checkbox");
		this.logoutButton = this.page.locator(".col .logout");
		this.Option = {
			Profile: this.page.getByTestId("profile-option"),
			ActionItems: this.page.getByTestId("action-items-option"),
			Mentions: this.page.getByTestId("mentions-option"),
		};
	}

	async selectOptionHamburgerMenu(option: Locator) {
		await this.hamburgerMenu.click();
		await option.click();
	}

	async logOut() {
		this.logoutButton.click();
	}
}

interface IOption {
	Profile: Locator;
	ActionItems: Locator;
	Mentions: Locator;
}
