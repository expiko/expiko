import { expect, Locator, Page } from "@playwright/test";

export class Profile {
	readonly page: Page;
	readonly cancelButton: Locator;
	readonly submitButton: Locator;
	readonly Field: IField;

	constructor(page: Page) {
		this.page = page;
		this.cancelButton = this.page.getByTestId("profile-cancel-button");
		this.submitButton = this.page.getByTestId("profile-submit-button");
		this.Field = {
			FirstName: this.page.locator("#first-name"),
			LastName: this.page.locator("#last-name"),
			Position: this.page.locator(".md-select-value[name=position-id]"),
			WorkExperience: this.page.locator("#workExperience"),
			Email: this.page.locator("#email"),
			Phone: this.page.locator("#phone"),
			Birthday: this.page.locator("#birthday .md-input"),
		};
	}

	async fillFirstName(firstName: string) {
		await this.Field.FirstName.fill(firstName);
	}

	async fillLastName(lastName: string) {
		await this.Field.LastName.fill(lastName);
	}

	async fillPosition(position: Position) {
		await this.Field.Position.click();
		await this.page.getByRole("button", { name: position }).click();
	}

	async fillPhone(phone: string) {
		await this.Field.Phone.fill(phone);
	}

	async fillWorkExperience(workExperience: string) {
		this.Field.WorkExperience.fill(workExperience);
	}

	async fillBirthday(birthday: string) {
		await this.Field.Birthday.fill(birthday);
		(await this.page.waitForSelector("text=Ok")).click();
	}

	async fillUserCredentials(firstName: string, lastName: string, phone: string) {
		await this.fillFirstName(firstName);
		await this.fillLastName(lastName);
		await this.fillPhone(phone);
	}

	async assertField(isToHaveValue: Boolean, field: Locator, value: string) {
		await this.isToHaveValue(isToHaveValue, field, value);
	}

	async pressCancelButton() {
		await this.cancelButton.click();
	}

	async pressSubmitButton() {
		await this.submitButton.click();
	}

	private async isToHaveValue(isToHaveValue: Boolean, field: Locator, value: string) {
		if (isToHaveValue) {
			await expect(field).toHaveValue(value);
		} else {
			await expect(field).not.toHaveValue(value);
		}
	}
}

export enum Position {
	SoftwareEngineer = "software engineer",
	QAEngineer = "QA engineer",
	ProjectManager = "project manager",
	ProductOwner = "product owner",
	Other = "other",
}

interface IField {
	FirstName: Locator;
	LastName: Locator;
	Position: Locator;
	WorkExperience: Locator;
	Email: Locator;
	Phone: Locator;
	Birthday: Locator;
}
