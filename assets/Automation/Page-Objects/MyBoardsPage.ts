import { expect, Locator, Page } from "@playwright/test";

export class MyBoardsPage {
	readonly page: Page;
	readonly addNewBoardButton: Locator;
	readonly projectNameField: Locator;
	readonly columnsNumberField: Locator;
	readonly columnsNameField: Locator;
	readonly startRetroButton: Locator;
	readonly pageTitle: Locator;
	private readonly boardDeleteButton: Locator;

	constructor(page: Page) {
		this.page = page;
		this.addNewBoardButton = this.page.getByTestId("add-new-board");
		this.projectNameField = this.page.getByTestId("new-board-project-name");
		this.columnsNumberField = this.page.getByTestId("new-board-columns-number");
		this.columnsNameField = this.page.getByTestId("new-board-column-names");
		this.startRetroButton = this.page.getByTestId("start-retro-button");
		this.pageTitle = this.page.locator(".blueprint .header .title");
		this.boardDeleteButton = this.page.locator("div.col:nth-child(2) > div:nth-child(1) > div:nth-child(3) > a:nth-child(1)").getByText("DELETE");
	}

	async fillBoardFields(projectName: string, columnsNumber: ColumnsNumber, columnsName: ColumnsName) {
		await this.addNewBoardButton.click();

		await this.projectNameField.fill(projectName);

		await this.columnsNumberField.click();
		await this.page.click(`text=- ${columnsNumber} -`);

		await this.columnsNameField.click();
		await this.page.click(`text=- ${columnsNumber} - ${columnsName}`);
	}

	async startRetro() {
		await this.startRetroButton.click();
	}

	async deleteLastCreatedBoard(confirmDelete: Boolean) {
		await this.boardDeleteButton.click();

		if (confirmDelete) {
			await this.page.getByRole("button", { name: "yes" }).click();
		} else {
			await this.page.getByRole("button", { name: "no" }).click();
		}
	}

	async toHaveTitle(title: string) {
		await expect(this.pageTitle).toHaveText(title);
	}
}

export enum ColumnsNumber {
	Three = 3,
	Four = 4,
}

export enum ColumnsName {
	GOOD_BAD_ACTIONS = "Good - Bad - Actions",
	WENTWELL_TOIMPROVE_ACTIONITEMS = "Went well - To improve - Action items",
	MAD_SAD_GLAD = "Mad - Sad - Glad",
	START_STOP_CONTINUE = "Start - Stop - Continue",
	GOOD_BAD_KEEP_ACTIONS = "Good - Bad - Keep - Actions",
	LIKED_LEARNED_LACKED_LONGED = "Liked - Learned - Lacked - Longed",
	KEEP_LESS_ADD_MORE = "Keep - Less - Add - More",
}
