import { expect, Locator, Page } from "@playwright/test";

export class Mentions {
	readonly page: Page;
	readonly pageTitle: Locator;

	constructor(page: Page) {
		this.page = page;
		this.pageTitle = this.page.getByTestId("mentions-title");
	}

	async toHaveTitle(title: string) {
		await expect(this.pageTitle).toHaveText(title);
	}

	async toHaveURL() {
		await expect(this.page).toHaveURL(this.page.url());
	}
}
