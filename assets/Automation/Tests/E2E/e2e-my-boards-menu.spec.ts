import { test } from "@playwright/test";
import { LoginPage } from "../../page-objects/LoginPage";
import { Header } from "../../page-objects/components/Header";
import { ActionItemsPage } from "../../page-objects/ActionItemsPage";
import { Mentions } from "../../page-objects/MentionsPage";

test.describe("Boards Menu", async () => {
	let loginPage: LoginPage;
	let header: Header;
	let actionItemsPage: ActionItemsPage;
	let mentions: Mentions;

	test.beforeEach(async ({ page }) => {
		loginPage = new LoginPage(page);
		header = new Header(page);
		actionItemsPage = new ActionItemsPage(page);
		mentions = new Mentions(page);

		await loginPage.visitMyRetro();
		await loginPage.login("masik.-@mail.ru", "5YjBJCsLfOTp47PKY2dh");
	});

	test("Check Action Items Menu Option", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.ActionItems);

		await actionItemsPage.toHaveTitle("Action Items");
		await actionItemsPage.toHaveURL();
	});

	test("Check Mentions Menu Option", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Mentions);

		await mentions.toHaveTitle("Mentions");
		await mentions.toHaveURL();
	});
});
