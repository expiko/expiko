import { test } from "@playwright/test";
import { LoginPage } from "../../page-objects/LoginPage";
import { ColumnsName, ColumnsNumber, MyBoardsPage } from "../../page-objects/MyBoardsPage";
import { Board, Column } from "../../page-objects/Board";

test.describe("Work With 3 Columns Board", async () => {
	let loginPage: LoginPage;
	let myBoardsPage: MyBoardsPage;
	let board: Board;

	test.beforeEach(async ({ page }) => {
		loginPage = new LoginPage(page);
		myBoardsPage = new MyBoardsPage(page);
		board = new Board(page);

		await loginPage.visitMyRetro();
		await loginPage.login("masik.-@mail.ru", "5YjBJCsLfOTp47PKY2dh");

		let boardTitleTestData = "New Board From Playwright";
		await myBoardsPage.fillBoardFields(boardTitleTestData, ColumnsNumber.Three, ColumnsName.GOOD_BAD_ACTIONS);
		await myBoardsPage.startRetro();

		await board.toHaveTitle(boardTitleTestData);
		await board.toHaveURL();
	});

	test.afterEach(async ({ page }) => {
		await page.goto("/");

		await myBoardsPage.deleteLastCreatedBoard(true);
	});

	test("Add a New Card in Good Column", async ({ page }) => {
		await board.addCard(Column.Good);

		let cardContentTestData = "test content";
		await board.typeInCard(cardContentTestData);
		await board.pressEnter();

		await board.assertCard(true, cardContentTestData);
	});

	test("Add a New Card in Bad Column", async ({ page }) => {
		await board.addCard(Column.Bad);

		let cardContentTestData = "test content";
		await board.typeInCard(cardContentTestData);
		await board.pressEnter();

		await board.assertCard(true, cardContentTestData);
	});
});
