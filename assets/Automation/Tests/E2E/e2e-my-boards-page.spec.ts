import { test, expect } from "@playwright/test";
import { LoginPage } from "../../page-objects/LoginPage";
import { ColumnsName, ColumnsNumber, MyBoardsPage } from "../../page-objects/MyBoardsPage";
import { Board } from "../../page-objects/Board";

test.describe("My Boards Page", async () => {
	let loginPage: LoginPage;
	let myBoardsPage: MyBoardsPage;
	let board: Board;

	test.beforeEach(async ({ page }) => {
		loginPage = new LoginPage(page);
		myBoardsPage = new MyBoardsPage(page);
		board = new Board(page);

		await loginPage.visitMyRetro();
		await loginPage.login("masik.-@mail.ru", "5YjBJCsLfOTp47PKY2dh");
	});

	test.afterEach(async ({ page }) => {
		await loginPage.visitMyRetro();

		await myBoardsPage.deleteLastCreatedBoard(true);
	});

	test("Create A New Board With 3 Columns", async ({ page }) => {
		let boardTitleTestData = "New Board From Playwright";
		await myBoardsPage.fillBoardFields(boardTitleTestData, ColumnsNumber.Three, ColumnsName.GOOD_BAD_ACTIONS);
		await myBoardsPage.startRetro();

		await board.toHaveTitle(boardTitleTestData);
		await board.toHaveURL();
	});

	test("Create A New Board With 4 Columns", async ({ page }) => {
		let boardTitleTestData = "New Board with 4 Columns From Playwright";
		await myBoardsPage.fillBoardFields(boardTitleTestData, ColumnsNumber.Four, ColumnsName.GOOD_BAD_KEEP_ACTIONS);
		await myBoardsPage.startRetro();

		await board.toHaveTitle(boardTitleTestData);
		await board.toHaveURL();
	});
});
