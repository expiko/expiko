import { test } from "@playwright/test";
import { LoginPage } from "../../page-objects/LoginPage";
import { Header } from "../../page-objects/components/Header";
import { MyBoardsPage } from "../../page-objects/MyBoardsPage";

test.describe.parallel("Login and logout", async () => {
	let loginPage: LoginPage;
	let header: Header;
	let myBoardsPage: MyBoardsPage;

	test.beforeEach(async ({ page }) => {
		loginPage = new LoginPage(page);
		header = new Header(page);
		myBoardsPage = new MyBoardsPage(page);

		await loginPage.visitMyRetro();
	});

	test("Login with invalid credentials", async ({ page }) => {
		await loginPage.login("wrong@mail.ru", "wrong password");

		await loginPage.assertErrorMessage();
	});

	test("Login and logout with valid credentials", async ({ page }) => {
		await loginPage.login("masik.-@mail.ru", "5YjBJCsLfOTp47PKY2dh");

		await myBoardsPage.toHaveTitle("My boards");

		await header.logOut();

		await loginPage.signInButtonToBeVisible();
	});
});
