import { test } from "@playwright/test";
import { LoginPage } from "../../page-objects/LoginPage";
import { Profile, Position } from "../../page-objects/components/Profile";
import { Header } from "../../page-objects/components/Header";

test.describe("User Profile", async () => {
	let loginPage: LoginPage;
	let profile: Profile;
	let header: Header;

	test.beforeEach(async ({ page }) => {
		loginPage = new LoginPage(page);
		profile = new Profile(page);
		header = new Header(page);

		await loginPage.visitMyRetro();
		await loginPage.login("masik.-@mail.ru", "5YjBJCsLfOTp47PKY2dh");
	});

	test("Change First name and Cancel", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let firstNameTestData = "Changed first name";
		await profile.fillFirstName(firstNameTestData);
		await profile.pressCancelButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(false, profile.Field.FirstName, firstNameTestData);
	});

	test("Change First name and Sumbit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let firstNameTestData = "Changed first name";
		await profile.fillFirstName(firstNameTestData);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.FirstName, firstNameTestData);
	});

	test("Change Last Name and Cancel", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let lastNameTestData = "Cancel first name";
		await profile.fillLastName(lastNameTestData);
		await profile.pressCancelButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(false, profile.Field.LastName, lastNameTestData);
	});

	test("Change Last Name and Submit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let lastNameTestData = "Changed First Name";
		await profile.fillLastName(lastNameTestData);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.LastName, lastNameTestData);
	});

	test("Check Email Field", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.Email, "masik.-@mail.ru");
	});

	test("Change Phone and Cancel", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let phoneTestData = "+79986664433";
		await profile.fillPhone(phoneTestData);
		await profile.pressCancelButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(false, profile.Field.Phone, phoneTestData);
	});

	test("Change Phone and Submit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let phoneTestData = "+79986664455";
		await profile.fillPhone(phoneTestData);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.Phone, phoneTestData);
	});

	test("Change Position and Submit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.fillPosition(Position.ProjectManager);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.Position, Position.ProjectManager);
	});

	test("Change Work Experience and Submit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let workExperienceTestData = "10";

		await profile.fillWorkExperience(workExperienceTestData);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.WorkExperience, workExperienceTestData);
	});

	test("Change Birthday and Submit", async ({ page }) => {
		await header.selectOptionHamburgerMenu(header.Option.Profile);

		let birthdayTestData = "1990-10-22";

		await profile.fillBirthday(birthdayTestData);
		await profile.pressSubmitButton();

		await header.selectOptionHamburgerMenu(header.Option.Profile);

		await profile.assertField(true, profile.Field.Birthday, birthdayTestData);
	});
});
