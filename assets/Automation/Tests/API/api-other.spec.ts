import { test, expect } from "@playwright/test";

test.describe("Other API endpoints testing", async () => {
	test("Get info about user", async ({ request }) => {
		let response = await request.get("me");

		let responseBody = JSON.parse(await response.text());
		console.log(responseBody);

		expect(response.status()).toBe(200);
		expect(responseBody.id).toBe(389);
		expect(responseBody.username).toBe("masik.-");
		expect(responseBody.email).toBe("masik.-@mail.ru");
	});

	test("Get template of board parameters", async ({ request }) => {
		let response = await request.get("templates");

		let responseBody = JSON.parse(await response.text());

		expect(response.status()).toBe(200);
		expect(responseBody[0].id).toBe(1);
		expect(responseBody[0].columns[0].id).toBe(1);
		expect(responseBody[0].columns[0].name).toBe("Good");

		console.log(responseBody);
	});
});
