import { test, expect } from "@playwright/test";

test.describe("API Tests", async () => {
	test("First API Test", async ({ request }) => {
		let response = await request.get("me");
		expect(response.status()).toBe(200);

		let responseBody = JSON.parse(await response.text());
		console.log(responseBody);
	});
});
