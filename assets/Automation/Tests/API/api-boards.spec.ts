import { test, expect } from "@playwright/test";

let _boardId: number;

test.describe("Boards API endpoints testing", async () => {
	test("Add new Board", async ({ request }) => {
		let boardNameTestData = "API Test Board from Playwright";

		let response = await request.post("boards", {
			data: {
				name: boardNameTestData,
				templateId: 1,
				description: "",
			},
		});

		let responseBody = JSON.parse(await response.text());

		expect(response.status()).toBe(200);
		expect(responseBody.name).toBe(boardNameTestData);
		expect(responseBody.description).toBe("");
		expect(responseBody.columns[0].name).toBe("Good");
		expect(responseBody.columns[1].name).toBe("Bad");
		expect(responseBody.columns[2].name).toBe("Actions");

		setBoardId(responseBody.id);
		console.log("boardId: " + getBoardId());
	});

	test("Add new Board with invalid templateId", async ({ request }) => {
		let boardNameTestData = "API Test Board from Playwright";

		let response = await request.post("boards", {
			data: {
				name: boardNameTestData,
				templateId: 0,
				description: "",
			},
		});

		let responseBody = JSON.parse(await response.text());

		expect(response.status()).toBe(400);
		expect(responseBody.error).toBe("Bad Request");
		expect(responseBody.message).toBe("Invalid board template");
		console.log(responseBody);
	});
});

test.afterAll(async ({ request }) => {
	await deleteExistingBoard(request);
});

let deleteExistingBoard = async function (request: any) {
	let response = await request.delete(`boards/${getBoardId()}`);

	expect(response.status()).toBe(200);
	console.log("afterAll boardId: " + getBoardId());
};

let getBoardId = function (): number {
	return _boardId;
};

let setBoardId = function (boardId: number) {
	_boardId = boardId;
};
