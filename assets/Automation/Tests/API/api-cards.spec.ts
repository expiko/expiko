import { test, expect } from "@playwright/test";

let _cardId: number;

test.describe.only("Cards API endpoints testing", async () => {
	test("Add new Card", async ({ request }) => {
		let cardTestData: string = "kartochka";

		let response = await request.post("cards", {
			data: {
				columnId: 5737,
				content: cardTestData,
			},
		});

		let responseBody = JSON.parse(await response.text());

		expect(responseBody.content).toBe(cardTestData);

		setCardId(responseBody.id);
		console.log(responseBody);
	});
});

test.afterAll(async ({ request }) => {
	await deleteExistingCard(request);

	console.log("afterAll cardId: " + getCardId());
});

let deleteExistingCard = async function (request: any) {
	let response = await request.delete(`cards/${getCardId()}`);

	expect(response.status()).toBe(200);
};

let getCardId = function (): number {
	return _cardId;
};

let setCardId = function (cardId: number) {
	_cardId = cardId;
};
