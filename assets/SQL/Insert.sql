
-- Добавление данных в таблицу Students

INSERT INTO trainsql.students (full_name, is_active)
VALUES('Ivan Ivanov', false);
INSERT INTO trainsql.students (full_name, is_active)
VALUES('Petr Petrov', false);
INSERT INTO trainsql.students (full_name, is_active)
VALUES('Adnrew Ivanov', false);
INSERT INTO trainsql.students (full_name, is_active)
VALUES('Stepan Stepanov', true);
INSERT INTO trainsql.students (full_name, is_active)
VALUES('Fedor Fedorov', true);

-- Добавление данных в таблицу Courses

INSERT INTO trainsql.courses (name, duration)
VALUES('Math', 100);
INSERT INTO trainsql.courses (name, duration)
VALUES('English', 50);
INSERT INTO trainsql.courses (name, duration)
VALUES('History', 88);

-- Добавление данных в таблицу Student Course

INSERT INTO trainsql.student_course (student_id, course_id, enrollment_date)
VALUES(4, 1, '2020-01-02');
INSERT INTO trainsql.student_course (student_id, course_id, enrollment_date)
VALUES(4, 3, '2020-02-10');
INSERT INTO trainsql.student_course (student_id, course_id, enrollment_date)
VALUES(5, 2, '2019-08-21');
INSERT INTO trainsql.student_course (student_id, course_id, enrollment_date)
VALUES(5, 1, '2019-07-5');
