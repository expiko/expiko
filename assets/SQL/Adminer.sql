
-- Найти like по какому-то конкретному likeid

SELECT * FROM public.like WHERE id = 15627;

-- Выбрать все лайки, где дата создания лайка больше даты старта обучения 

SELECT * FROM public.like WHERE "createDate" > '2023-03-13' LIMIT 10;

-- Выбрать все комментарии пользователя

SELECT * FROM public.comment WHERE "userId" = 389 LIMIT 10;

-- Запрос выводящий 4 столбца из двух таблиц - UsedID, Username CardID, Content

SELECT c."userId", uv.username, c."cardId", c.content
FROM public.comment c, public.user_view uv
WHERE c."userId" = uv.id LIMIT 10;