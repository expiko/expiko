
-- Подсчет количества покупателей из каждой страны

SELECT c.cust_country AS country,
	   count(*) AS customers
FROM learnsql.customers c
WHERE c.cust_country IS NOT NULL
GROUP BY cust_country;

-- Вывод всех записей о вендорах у которых нет ни одного продукта

SELECT DISTINCT * FROM learnsql.vendors v
WHERE v.vend_id NOT IN (SELECT p.vend_id FROM learnsql.products p);

-- Подсчет всех покупателей кто сделал самый дорогостоящий заказ в феврале 2020 года

SELECT c.cust_name AS Customer,
	   c.cust_email AS Email,
	   sum(oi.item_price * oi.quantity) AS Price
FROM learnsql.customers c
INNER JOIN learnsql.orderitems oi 
WHERE c.cust_id IN
	(SELECT o.cust_id FROM learnsql.orders o
	WHERE (o.order_date BETWEEN '2020-02-01' AND '2020-02-29') AND o.order_num IN
		(SELECT oi.order_num
		FROM learnsql.orderitems))
GROUP BY Customer, Email
ORDER BY Price DESC;

