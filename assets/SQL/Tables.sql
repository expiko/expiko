
-- Создание в БД три таблицы:

-- students со столбцами id, full_name, age, country, city, is_active

CREATE TABLE trainsql.students (
	id SMALLINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	full_name varchar(200) NOT NULL,
	age TINYINT UNSIGNED,
	country varchar(150),
	city varchar(150),
	is_active bool NOT NULL);

-- courses со столбцами id, name, description, duration

CREATE TABLE trainsql.courses (
	id SMALLINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	name varchar(150) NOT NULL,
	description varchar(500),
	duration TINYINT NOT null);

-- student_course со столбцами student_id и course_id, enrollment_date

CREATE TABLE trainsql.student_course (
	student_id SMALLINT NOT NULL,
	course_id SMALLINT NOT NULL,
	enrollment_date date NOT NULL);

-- Изменение таблиц:
-- students - увеличить поле full_name на 200 символов, удалить столбец age
    
ALTER TABLE trainsql.students
MODIFY COLUMN full_name varchar(200) NOT NULL,
DROP COLUMN age;
    
-- courses - добавить текстовое поле teacher_name длиной 400 символов
    
ALTER TABLE trainsql.courses
ADD COLUMN teacher_name varchar(400);
    
-- student_course - добавить поле completion_date, содержащее дату в формате YYYY-MM-DD

ALTER TABLE trainsql.student_course
ADD COLUMN completion_date date NULL;

-- Удаление таблицы student_course

DROP TABLE trainsql.student_course;
